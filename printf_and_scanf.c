//learing how to use printf and scanf predefined functions
#include<stdio.h>
#include<stdlib.h>
int main()
{
    int x;                  //int data type
    float y;                //float data type
    char z;                 //charater data type
    char str[100];          //string 
    printf("Enter integral value x: ");
    scanf("%d", &x);
    printf("Enter Decimal value y: ");
    scanf("%f", &y);
    printf("Enter character value of z: ");
    scanf("%c", &z);
    printf("Enter string: ");
    scanf("%s", str);
    
    printf("The Values you entered are: \n");
    printf("x=%d\n", x);
    printf("y=%f\n", y);
    printf("z=%c\n", z);
    printf("str = %s", str);
    
    return 0;
}
