// basic C program                    //Documentation section 
#include<stdio.h>                     //preprocessor section
#define N 100                         //definition section 
int X = 200;
float Z = 300.5;
char name[100] = "John";
void user_fun();
void user_fun()
{
    printf("X = %d is a macro (globally declared)\n", X);
    printf("Z= %.2f, and name = %s are global variables\n",Z, name);
}
int main()
{
    int a = 1, b = 2;
    printf("a = %d and b = %d are local variables\n", a, b);
    user_fun();
    return 0;
}


